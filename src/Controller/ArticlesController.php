<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
   /* public function index()
    {
        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }*/
    
    public function initialize()
    {
      parent::initialize();

      $this->loadComponent('Flash'); // Include the FlashComponent
    }
    
    public function index()
    {
      $this->set('articles', $this->Articles->find('all'));
    }



    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
  
    /*public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);
    }*/

    public function view($id)
    {
      $article = $this->Articles->get($id);
      $this->set(compact('article'));
    }
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    
    public function add()
    {
      $article = $this->Articles->newEntity();
      if ($this->request->is('post')) {
        $article = $this->Articles->patchEntity($article, $this->request->data);
        if ($this->Articles->save($article)) {
          $this->Flash->success(__('記事を投稿しました。'));
          return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('投稿できませんでした。'));
      }
      $this->set('article', $article);
      
      // Just added the categories list to be able to choose
      // one category for an article
      $categories = $this->Articles->Categories->find('treeList');
      $this->set(compact('categories'));
    }
  
  
    
    /*public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('article'));
        $this->set('_serialize', ['article']);
    }*/

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    
    public function edit($id = null)
    {
      $article = $this->Articles->get($id);
      if ($this->request->is(['post', 'put'])) {
        $this->Articles->patchEntity($article, $this->request->data);
        if ($this->Articles->save($article)) {
          $this->Flash->success(__('記事を編集しました。'));
          return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('編集出来ませんでした。'));
      }

      $this->set('article', $article);
    }
    
    /*public function edit($id = null)
    {
        $article = $this->Articles->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been updated.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Unable to updated your article.'));
            }
        }
        $this->set('article',$article);
    }*/

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('記事番号:'.$id{0}.'を削除しました。'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
