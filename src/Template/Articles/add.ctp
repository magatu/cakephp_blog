<!-- File: src/Template/Articles/add.ctp -->


<h1>記事投稿</h1>
<?php
echo $this->Form->create($article);
echo $this->Form->input('title',['placeholder' => 'タイトルを入れろ']);
echo $this->Form->input('body', ['rows' => '10','placeholder' => '記事本文入れろ','wrap' => 'hard']);
echo $this->Form->button(__('投稿'));
echo $this->Form->end();
?>