<!-- File: src/Template/Articles/edit.ctp -->


<h1>記事編集</h1>
<?php 
    echo $this->Form->create($article);
    echo $this->Form->input('title',['placeholder' => 'タイトル入れろ']);
    echo $this->Form->input('body',['rows' => '10','placeholder' => '本文いれろ','wrap' => 'hard']);
    echo $this->Form->button(__('保存'));
    echo $this->Form->end();
?>