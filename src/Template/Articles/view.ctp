<!-- File: src/Template/Articles/view.ctp -->

<h1><?= h($article->title) ?></h1>
<p><?= nl2br(h($article->body)) ?></p> <!-- 改行して表示 -->
<p><small>Created: <?= $article->created->format(DATE_RFC850) ?></small></p>
